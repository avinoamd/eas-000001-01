/*
 * ItemScannerSerialComm.c
 *
 *  Created on: May 6, 2021
 *      Author: segev.gofin
 */
#include "FreeRTOS.h"
#include "cmsis_os2.h"
#include "task.h"
#include "queue.h"
#include "usart.h"
#include "ItemScannerSerialComm.h"
#include "timers.h"
#include "semphr.h"
#include "main.h"
#include <string.h>
#include <stdlib.h>

// Bit macro
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define bitSet(value, bit) ((value) |= (1UL << (bit)))
#define bitClear(value, bit) ((value) &= ~(1UL << (bit)))
#define bitWrite(value, bit, bitvalue) ((bitvalue) ? bitSet(value, bit) : bitClear(value, bit))

//Task ItemScannerSerialComm
uint8_t numberISHandlers = 0;

///Queue Serial communication RX
//osMessageQueueId_t QueueModbusHandle;
const osMessageQueueAttr_t QueueItemScanner_attributes = {
       .name = "ItemScannerItemScanner"
};

//Sempahore to access the modebus Data
const osSemaphoreAttr_t ItemScannerSemap_attributes = {
    .name = "ItemScannerSerialCommSemap"
};

/* Definitions for IO_task */
//osThreadId_t ItemScanner_taskHandle;
const osThreadAttr_t ItemScannerRx_task_attributes = {
  .name = "ItemScannerRxSerialComm_task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
const osThreadAttr_t ItemScannerTx_task_attributes = {
  .name = "ItemScannerTxSerialComm_task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/**
 * Initialization for Serial Comm task
 * */
void ItemScannerSerialCommInit(ItemScannerHandler_t * ItemScannerTaskHandler)
{
	//Create Queue and Thread Rx
	ItemScannerTaskHandler->QueueSerialHandle = osMessageQueueNew (MAX_BUFFER, sizeof(uint8_t), &QueueItemScanner_attributes);
	ItemScannerTaskHandler->ItemScannerCommTaskRx = osThreadNew(StartItemScannerRx_task, ItemScannerTaskHandler, &ItemScannerRx_task_attributes);
	/* Create Queue and Thread Tx */
	/*ItemScannerTaskHandler->ItemScannerCommTaskTx = osThreadNew(StartItemScannerTx_task,ItemScannerTaskHandler,&ItemScannerTx_task_attributes);
	if (ItemScannerTaskHandler->ItemScannerCommTaskTx == NULL)
		ItemScannerTaskHandler->ItemScannerCommTaskTx = osThreadNew(StartItemScannerTx_task,ItemScannerTaskHandler,&ItemScannerTx_task_attributes);
	*///Create Semaphore DataRX
	 //vSemaphoreCreateBinary(SemaphoreDataRX);
	 //Create timer T35

	ItemScannerTaskHandler->xTimerT35 = xTimerCreate("TimerT35",         // Just a text name, not used by the kernel.
			  	  	  	  	  	  	  	5 ,     // The timer period in ticks.
	                                    pdFALSE,         // The timers will auto-reload themselves when they expire.
										( void * )ItemScannerTaskHandler->xTimerT35,     // Assign each timer a unique id equal to its array index.
	                                    (TimerCallbackFunction_t) vItemScannerTimerCallbackT35     // Each timer calls the same callback when it expires.
	                                    );
	IHandlers[numberISHandlers] = ItemScannerTaskHandler;
	numberISHandlers++;

}


void ItemScannerSerialCommStart(ItemScannerHandler_t * ItemScannerTaskHandler)
{
    //check that port is initialized
    while (HAL_UART_GetState(ItemScannerTaskHandler->port) != HAL_UART_STATE_READY)
    {
    }

    // Receive data from serial port for Serial communication using interrupt
    if(HAL_UART_Receive_IT(ItemScannerTaskHandler->port, &ItemScannerTaskHandler->dataRX, 1) != HAL_OK)
    {
        while(1)
        {
        }
    }
    ItemScannerTaskHandler->u8BufferSize = 0;
/*
    ItemScannerTaskHandler->u8lastRec = ItemScannerTaskHandler->u8BufferSize = 0;
    ItemScannerTaskHandler->u16InCnt = ItemScannerTaskHandler->u16OutCnt = modH->u16errCnt = 0;
 */
}

/* creation of Serial communication Rx task */
void StartItemScannerRx_task(void *argument)
{

	const char * ptr_start;	// start of item scan
	char * ptr_end;		// end of item scan
	ItemScannerHandler_t * ItemScannerHandler = (ItemScannerHandler_t * )argument;

	  /* Infinite loop */
	for(;;)
	{
	  /* Use ulTaskNotifyTake() to [optionally] block to wait for a task’s notification value to be non-zero.
	   * The task does not consume any CPU time while it is in the Blocked state */
	  ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
	  ItemScannerHandler->u8BufferSize = uxQueueMessagesWaiting(ItemScannerHandler->QueueSerialHandle);

	  int8_t i8state = getItemScannerRxBuffer(ItemScannerHandler);

	  if (i8state < 7){
		  //The size of the frame is invalid
		  xQueueGenericReset(ItemScannerHandler->QueueSerialHandle, pdFALSE);
		  continue;
	  }
	  ptr_start = (const char*)ItemScannerHandler->au8Buffer;
	  //ptr_start = (const char*)strchr(ptr_start,0x2)+1;	//find start of transmit
	  ptr_end = strchr(ptr_start,'\r');
	  *ptr_end = '\0';// cut \r a the end of
	  char** ptr_ptr_end = &ptr_end;
	  // str = str[strcspn(str,"\r")];

	  unsigned long long int item_barcode  = strtoull(ptr_start,ptr_ptr_end,10);
	  osSemaphoreAcquire (*ItemScannerHandler->ItemScannerSemap_HandleRx , portMAX_DELAY); //before processing the message get the semaphore
	  ItemScannerHandler->Data[ItemScannerHandler->ItemScanner_read_position_MSB] = (uint16_t) (item_barcode >> 48);
	  ItemScannerHandler->Data[ItemScannerHandler->ItemScanner_read_position_B2] = (uint16_t) ((item_barcode<<16) >> 48);
	  ItemScannerHandler->Data[ItemScannerHandler->ItemScanner_read_position_B3] = (uint16_t) ((item_barcode<<32) >> 48); // place the ItemScanner reading into Data array at register eModbusMap_ItemScannerReading
	  ItemScannerHandler->Data[ItemScannerHandler->ItemScanner_read_position_LSB] = (uint16_t) ((item_barcode<<48)>>48);
	  osSemaphoreRelease(*ItemScannerHandler->ItemScannerSemap_HandleRx); //Release the semaphore
	  osDelay(1);
	}
}

/* creation of Serial communication Tx task */
/*
void StartItemScannerTx_task(void *argument)
{

	ItemScannerHandler_t * ItemScannerHandler = (ItemScannerHandler_t * )argument;
	uint32_t CommandReadFromPLC=0;
	uint32_t cycleTime;

	for(;;)
	{
		CommandReadFromPLC = ItemScannerHandler->Data[eModbusMap_ReadCycleTime];
		// if PLC doesn't give cycleTime then eDefaultTxCycleTime is the cycle time
		if (CommandReadFromPLC && osKernelGetTickFreq () != 0)
			cycleTime = (CommandReadFromPLC * osKernelGetTickFreq () / 1000);
		else
			cycleTime = eDefaultTxCycleTime;
		// Wait for a specified time period in ticks, While the system waits, the thread that is calling this function is put into the state WAITING
		//ulTaskNotifyTake(pdTRUE,cycleTime);
		if (bitRead(ItemScannerHandler->Data[eModbusMap_ReadRequest],ItemScannerHandler->bit_read_req))
			sendTxItemScanner(ItemScannerHandler);

		osDelay(cycleTime);
	}

}

void sendTxItemScanner(ItemScannerHandler_t *ItemScannerHandler)
{

	// transfer buffer to serial line
	HAL_UART_Transmit_IT(ItemScannerHandler->port, ItemScannerHandler->WeightReadReqAssci,  sizeof( ItemScannerHandler->WeightReadReqAssci));
//	ulTaskNotifyTake(pdTRUE, portMAX_DELAY); //wait notification from TXE interrupt
*//*
	osSemaphoreAcquire (*ItemScannerHandler->ItemScannerSemap_HandleTx , portMAX_DELAY); //before processing the message get the semaphore
	bitWrite( ItemScannerHandler->Data[eModbusMap_ReadRequest],eReadRequestMap_ReadReq,1); // place the ItemScanner reading into Data array at register eModbusMap_ItemScannerReading
	osSemaphoreRelease(*ItemScannerHandler->ItemScannerSemap_HandleTx); //Release the semaphore
*//*
}

*/
/**
 * @brief
 * This method moves Serial buffer data to the ItemScannerSerial au8Buffer.
 *
 */
int8_t getItemScannerRxBuffer(ItemScannerHandler_t *ItemScannerHandler)
{
    int i;

    ItemScannerHandler->u8BufferSize = uxQueueMessagesWaiting(ItemScannerHandler->QueueSerialHandle);

    for(i = 0; i<  ItemScannerHandler->u8BufferSize; i++ )
   	{
   		  xQueueReceive(ItemScannerHandler->QueueSerialHandle, &ItemScannerHandler->au8Buffer[i], 0);
   	}

    return ItemScannerHandler->u8BufferSize;
}

void vItemScannerTimerCallbackT35(TimerHandle_t *pxTimer)
{
	//Notify that a steam has just arrived
	int i;
	//TimerHandle_t aux;
	for(i = 0; i < numberISHandlers; i++)
	{

		if( (TimerHandle_t *)IHandlers[i]->xTimerT35 ==  pxTimer ){
			xTaskNotify(IHandlers[i]->ItemScannerCommTaskRx, 0, eSetValueWithOverwrite);
		}

	}
}
