/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
EAS-000001-01
*/
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include <stdio.h>
//#include "IO.h"
//#include "maintest.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
modbusHandler_t ModbusH;
ItemScannerHandler_t hBarcode1, hBarcode2;			// BARCODE
uint16_t ModbusDATA[21];
char buff[50], buff2[50];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */
/*
  HAL_StatusTypeDef res;
  int cnt = 0;
  HAL_UART_ReceiverTimeout_Config(&huart2, 60);						// timeout in end of message
  res = HAL_UART_EnableReceiverTimeout(&huart2);					// timeout in end of message

  while(1)
  {
	  res = HAL_UART_Receive(&huart2, buff, 40, 0x7fffffff);
	  {
		  sprintf(buff2, "           %d\n", cnt++);
		  HAL_UART_Transmit(&huart1, buff, strlen(buff), 100);
		  HAL_UART_Transmit(&huart1, buff2, strlen(buff2), 100);
		  printf(buff);
		  printf(buff2);
	  }
  }
*/
  /* Modbus Slave initialization over RS-485*/
	ModbusH.uModbusType = MB_SLAVE;
	ModbusH.port =  &huart1; // This is the UART port connected to STLINK
	ModbusH.u8id = 3; //slave ID
	ModbusH.u16timeOut = 1000;
	ModbusH.EN_Port = RS485_DE_GPIO_Port;
	ModbusH.EN_Pin = RS485_DE_Pin; // RS485 Enable
	ModbusH.EN_Port = NULL;
	ModbusH.EN_Pin = NULL; // RS485 Enable
	ModbusH.u32time = 0;
	ModbusH.u16regs = ModbusDATA;
	ModbusH.u16regsize= sizeof(ModbusDATA)/sizeof(ModbusDATA[0]);
	ModbusH.xTypeHW	=	USART_HW;

//	osKernelInitialize();  /* Call init function for freertos objects (in freertos.c) */
//	MX_FREERTOS_Init();

	//Initialize Modbus library
	ModbusInit(&ModbusH);
	//Start capturing traffic on serial Port
	ModbusStart(&ModbusH);


	/***	Init barcode 1		***/
	hBarcode1.Data = ModbusDATA;
	hBarcode1.ItemScannerSemap_HandleRx = &(ModbusH.ModBusSphrHandle);
	hBarcode1.ItemScannerSemap_HandleTx = &(ModbusH.ModBusSphrHandle);
	hBarcode1.port = &huart2;
	//hBarcode1.bit_read_req = (uint8_t)eReadRequestMap_ReadReq;
	hBarcode1.ItemScanner_read_position_MSB = (uint8_t)eModbusMap_scanner1_MSB;
	hBarcode1.ItemScanner_read_position_B2 = (uint8_t)eModbusMap_scanner1_B2;
	hBarcode1.ItemScanner_read_position_B3 = (uint8_t)eModbusMap_scanner1_B3;
	hBarcode1.ItemScanner_read_position_LSB = (uint8_t)eModbusMap_scanner1_LSB;
	//Initialize tem barcode serial communication library
	ItemScannerSerialCommInit(&hBarcode1);
	//Start capturing traffic on serial Port
	ItemScannerSerialCommStart(&hBarcode1);

	/***	Init barcode 2		***/
	hBarcode2.Data = ModbusDATA;
	hBarcode2.ItemScannerSemap_HandleRx = &(ModbusH.ModBusSphrHandle);
	hBarcode2.ItemScannerSemap_HandleTx = &(ModbusH.ModBusSphrHandle);
	hBarcode2.port = &huart3;
	//hBarcode1.bit_read_req = (uint8_t)eReadRequestMap_ReadReq;
	hBarcode2.ItemScanner_read_position_MSB = (uint8_t)eModbusMap_scanner2_MSB;
	hBarcode2.ItemScanner_read_position_B2 = (uint8_t)eModbusMap_scanner2_B2;
	hBarcode2.ItemScanner_read_position_B3 = (uint8_t)eModbusMap_scanner2_B3;
	hBarcode2.ItemScanner_read_position_LSB = (uint8_t)eModbusMap_scanner2_LSB;
	//Initialize tem barcode serial communication library
	ItemScannerSerialCommInit(&hBarcode2);
	//Start capturing traffic on serial Port
	ItemScannerSerialCommStart(&hBarcode2);


  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();
  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 24;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM2 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM2) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
