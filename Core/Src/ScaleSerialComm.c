/*
 * ScaleSerialComm.c
 *
 *  Created on: Sep 14, 2020
 *      Author: segev.gofin
 */
#include "FreeRTOS.h"
#include "cmsis_os2.h"
#include "task.h"
#include "queue.h"
#include "usart.h"
#include "ScaleSerialComm.h"
#include "timers.h"
#include "semphr.h"
#include "main.h"
#include <stdlib.h>

// Bit macro
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define bitSet(value, bit) ((value) |= (1UL << (bit)))
#define bitClear(value, bit) ((value) &= ~(1UL << (bit)))
#define bitWrite(value, bit, bitvalue) ((bitvalue) ? bitSet(value, bit) : bitClear(value, bit))

//Task ScaleSerialComm
uint8_t numberTHandlers = 0;

///Queue Serial communication RX
//osMessageQueueId_t QueueModbusHandle;
const osMessageQueueAttr_t QueueScale_attributes = {
       .name = "QueueScale"
};

//Sempahore to access the modebus Data
const osSemaphoreAttr_t ScaleSemap_attributes = {
    .name = "ScaleSerialCommSemap"
};

/* Definitions for IO_task */
//osThreadId_t Scale_taskHandle;
const osThreadAttr_t ScaleRx_task_attributes = {
  .name = "ScaleRxSerialComm_task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
const osThreadAttr_t ScaleTx_task_attributes = {
  .name = "ScaleTxSerialComm_task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/**
 * Initialization for Serial Comm task
 * */
void ScaleSerialCommInit(ScaleHandler_t * ScaleTaskHandler)
{
	//Create Queue and Thread Rx
	ScaleTaskHandler->QueueSerialHandle = osMessageQueueNew (MAX_BUFFER, sizeof(uint8_t), &QueueScale_attributes);
	ScaleTaskHandler->ScaleCommTaskRx = osThreadNew(StartScaleRx_task, ScaleTaskHandler, &ScaleRx_task_attributes);
	/* Create Queue and Thread Tx */
	ScaleTaskHandler->ScaleCommTaskTx = osThreadNew(StartScaleTx_task,ScaleTaskHandler,&ScaleTx_task_attributes);
	if (ScaleTaskHandler->ScaleCommTaskTx == NULL)
		ScaleTaskHandler->ScaleCommTaskTx = osThreadNew(StartScaleTx_task,ScaleTaskHandler,&ScaleTx_task_attributes);
	//Create Semaphore DataRX
	 //vSemaphoreCreateBinary(SemaphoreDataRX);
	 //Create timer T35

	ScaleTaskHandler->xTimerT35 = xTimerCreate("TimerT35",         // Just a text name, not used by the kernel.
			  	  	  	  	  	  	  	5 ,     // The timer period in ticks.
	                                    pdFALSE,         // The timers will auto-reload themselves when they expire.
										( void * )ScaleTaskHandler->xTimerT35,     // Assign each timer a unique id equal to its array index.
	                                    (TimerCallbackFunction_t) vScaleTimerCallbackT35     // Each timer calls the same callback when it expires.
	                                    );
	tHandlers[numberTHandlers] = ScaleTaskHandler;
	numberTHandlers++;
	ScaleTaskHandler->WeightReadReqAssci[0] = 0x0D; // Carriage Return '/r'
	ScaleTaskHandler->WeightReadReqAssci[1] = 0x77; // hex number request reading from scale
	ScaleTaskHandler->WeightReadReqAssci[2] = 0x0A;	// Line Feed '\n'
	ScaleTaskHandler->WeightReadReqAssci[3] = 0;
}


void ScaleSerialCommStart(ScaleHandler_t * ScaleTaskHandler)
{
    //check that port is initialized
    while (HAL_UART_GetState(ScaleTaskHandler->port) != HAL_UART_STATE_READY)
    {
    }

    // Receive data from serial port for Serial communication using interrupt
    if(HAL_UART_Receive_IT(ScaleTaskHandler->port, &ScaleTaskHandler->dataRX, 1) != HAL_OK)
    {
        while(1)
        {
        }
    }
    ScaleTaskHandler->u8BufferSize = 0;
/*
    ScaleTaskHandler->u8lastRec = ScaleTaskHandler->u8BufferSize = 0;
    ScaleTaskHandler->u16InCnt = ScaleTaskHandler->u16OutCnt = modH->u16errCnt = 0;
 */
}

/* creation of Serial communication Rx task */
void StartScaleRx_task(void *argument)
{
	char *ptr;
	const char *str;

	ScaleHandler_t * ScaleHandler = (ScaleHandler_t * )argument;

	  /* Infinite loop */
	for(;;)
	{
	  /* Use ulTaskNotifyTake() to [optionally] block to wait for a task’s notification value to be non-zero.
	   * The task does not consume any CPU time while it is in the Blocked state */
	  ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
	  ScaleHandler->u8BufferSize = uxQueueMessagesWaiting(ScaleHandler->QueueSerialHandle);

	  int8_t i8state = getScaleRxBuffer(ScaleHandler);

	  if (i8state < 7)
	  {
		  //The size of the frame is invalid
		  xQueueGenericReset(ScaleHandler->QueueSerialHandle, pdFALSE);
		  continue;
	  }
	  str = (const char*)(ScaleHandler->au8Buffer + 1);
	  uint8_t modbus_place = 0;
	  // test and figure out where to place the reading in the modbus array
	  for (int i = 0;i < numberTHandlers;i++)
	  {
		  if (tHandlers[i]->port == ScaleHandler->port)
		  {
			  modbus_place = tHandlers[i]->scale_read_position;
			  break;
		  }
	  }
	  // place the reading in the correct modbus array index
	  // if (modbus_place != 0)
	  {
		  osSemaphoreAcquire (*ScaleHandler->ScaleSemap_HandleRx , portMAX_DELAY); //before processing the message get the semaphore
		  ScaleHandler->Data[modbus_place] = (uint16_t) (1000 * strtof(str,&ptr)); // place the scale reading into Data array at register eModbusMap_ScaleReading
		  osSemaphoreRelease(*ScaleHandler->ScaleSemap_HandleRx); 						//Release the semaphore
	  }
	  osDelay(500);
	}
}

/* creation of Serial communication Tx task */
void StartScaleTx_task(void *argument)
{

	ScaleHandler_t * ScaleHandler = (ScaleHandler_t * )argument;
	uint32_t CommandReadFromPLC=0;
	uint32_t cycleTime;

	for(;;)
	{
		CommandReadFromPLC = ScaleHandler->Data[eModbusMap_ReadCycleTime];
		// if PLC doesn't give cycleTime then eDefaultTxCycleTime is the cycle time
		if (CommandReadFromPLC && osKernelGetTickFreq () != 0)
			cycleTime = (CommandReadFromPLC * osKernelGetTickFreq () / 1000);
		else
			cycleTime = eDefaultTxCycleTime;
		// Wait for a specified time period in ticks, While the system waits, the thread that is calling this function is put into the state WAITING
		//ulTaskNotifyTake(pdTRUE,cycleTime);
		if (bitRead(ScaleHandler->Data[eModbusMap_ReadRequest],ScaleHandler->bit_read_req))
			sendTxScale(ScaleHandler);

		osDelay(cycleTime);
	}

}

void sendTxScale(ScaleHandler_t *ScaleHandler)
{

	// transfer buffer to serial line
	HAL_UART_Transmit_IT(ScaleHandler->port, ScaleHandler->WeightReadReqAssci,  sizeof( ScaleHandler->WeightReadReqAssci));
//	ulTaskNotifyTake(pdTRUE, portMAX_DELAY); //wait notification from TXE interrupt
/*
	osSemaphoreAcquire (*ScaleHandler->ScaleSemap_HandleTx , portMAX_DELAY); //before processing the message get the semaphore
	bitWrite( ScaleHandler->Data[eModbusMap_ReadRequest],eReadRequestMap_ReadReq,1); // place the scale reading into Data array at register eModbusMap_ScaleReading
	osSemaphoreRelease(*ScaleHandler->ScaleSemap_HandleTx); //Release the semaphore
*/
}


/**
 * @brief
 * This method moves Serial buffer data to the ScaleSerial au8Buffer.
 *
 */
int8_t getScaleRxBuffer(ScaleHandler_t *ScaleHandler)
{
    int i;

    ScaleHandler->u8BufferSize = uxQueueMessagesWaiting(ScaleHandler->QueueSerialHandle);

    for(i = 0; i<  ScaleHandler->u8BufferSize; i++ )
   	{
   		  xQueueReceive(ScaleHandler->QueueSerialHandle, &ScaleHandler->au8Buffer[i], 0);
   	}

    return ScaleHandler->u8BufferSize;
}

void vScaleTimerCallbackT35(TimerHandle_t *pxTimer)
{
	//Notify that a steam has just arrived
	int i;
	//TimerHandle_t aux;
	for(i = 0; i < numberHandlers; i++)
	{

		if( tHandlers[i]->xTimerT35 ==  pxTimer )
		{
			xTaskNotify(tHandlers[i]->ScaleCommTaskRx, 0, eSetValueWithOverwrite);
		}

	}
}
