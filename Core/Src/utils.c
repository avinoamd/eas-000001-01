/*
 * utils.c
 *
 *  Created on: 26 Jul 2021
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "utils.h"

/**
 * @brief Initializes DWT_Clock_Cycle_Count for DWT_Delay_us function
 * @return Error DWT counter
 * 1: clock cycle counter not started
 * 0: clock cycle counter works
 */
uint32_t DWT_Delay_Init(void)
{
	 /* Disable TRC */
	 CoreDebug->DEMCR &= ~CoreDebug_DEMCR_TRCENA_Msk; // ~0x01000000;
	 /* Enable TRC */
	 CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk; // 0x01000000;
	 /* Disable clock cycle counter */
	 DWT->CTRL &= ~DWT_CTRL_CYCCNTENA_Msk; //~0x00000001;
	 /* Enable clock cycle counter */
	 DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk; //0x00000001;
	 /* Reset the clock cycle counter value */
	 DWT->CYCCNT = 0;
	/* 3 NO OPERATION instructions */
	__ASM volatile ("NOP");
	__ASM volatile ("NOP");
	 __ASM volatile ("NOP");
	 /* Check if clock cycle counter has started */
	if(DWT->CYCCNT)
	{
		return 0; /*clock cycle counter started*/
	}
	else
	{
		return 1; /*clock cycle counter not started*/
	}
}

int _write(int file, char *ptr, int len)
{
  /* Implement your write code here, this is used by puts and printf for example */
  int i;
  for(i=0 ; i<len ; i++)
    ITM_SendChar((*ptr++));
  return len;
}

__STATIC_INLINE uint32_t ITM_Out (uint32_t cnl, uint32_t ch)
{
  if (((ITM->TCR & ITM_TCR_ITMENA_Msk) != 0UL) &&      /* ITM enabled */
      ((ITM->TER & 1UL               ) != 0UL)   )     /* ITM Port #0 enabled */
  {
    while (ITM->PORT[cnl].u32 == 0UL)
    {
   	 __NOP();
    }
    ITM->PORT[cnl].u8 = (uint8_t)ch;
  }
  return (ch);
}

void swvPrint(int port, char* ptr, int len)
{
  int i = 0;

  for (i = 0; i < len; i++)
    ITM_Out(port, (uint32_t) *ptr++);
}


