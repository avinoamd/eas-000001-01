/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "Modbus.h"
#include "ScaleSerialComm.h"
#include "ItemScannerSerialComm.h"
//#include "SideScale1SerialComm.h"
//#include "SideScale2SerialComm.h"

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
extern modbusHandler_t ModbusH;
extern ItemScannerHandler_t hBarcode1, hBarcode2;
//extern SideScale1Handler_t SideScaleSerial_1;
//extern SideScale2Handler_t SideScaleSerial_2;
//extern ItemScannerHandler_t ItemScannerSerial;



enum ModbusMapEnum {
	// read
	eModbusMap_Scale_1_Reading = 0,	// reading from main scale
	eModbusMap_Scale_2_Reading,
	eModbusMap_Scale_3_Reading,
	eModbusMap_scanner1_MSB,			// read write
	eModbusMap_scanner1_B2,				// read write
	eModbusMap_scanner1_B3,				// read write
	eModbusMap_scanner1_LSB,			// read write
	eModbusMap_scanner2_MSB,			// read write
	eModbusMap_scanner2_B2,				// read write
	eModbusMap_scanner2_B3,				// read write
	eModbusMap_scanner2_LSB,			// read write
//	eModbusMap_HeartbeatFromPLC	=	0,
//	eModbusMap_HeartbeatToPLC	=	1,

//	eModbusMap_McuAdress,
	// write
	eModbusMap_ReadCycleTime, 			//cycle time to ask for reading from PLC
	eModbusMap_ReadRequest,

};



enum ReadRequestMap {
	eReadRequestMap_ReadReq = 0,		// Bit from PLC for asking reading
	eReadRequestMap_Scale_2,
	eReadRequestMap_Scale_3
};

/* A number that if the address in power up is equal to then MCU goes into test mode */
enum {eGoIntoTestModeAddress = 2};

typedef enum {
	eTest_state_RS232_Transmit,
	eTest_state_RS232_Receive,
	eTest_state_RS232_Compare,
	eTest_state_Blinker
}eTest_state;

enum {eDefaultTxCycleTime = 425};

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define RS485_DE_Pin GPIO_PIN_12
#define RS485_DE_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
