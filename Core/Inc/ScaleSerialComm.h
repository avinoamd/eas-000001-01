/*
 * ScaleSerialComm.h
 *
 *  Created on: Sep 14, 2020
 *      Author: segev.gofin
 */

#ifndef INC_SCALESERIALCOMM_H_
#define INC_SCALESERIALCOMM_H_

typedef struct
{
	uint16_t * Data; 								// pointer to Data from/to modbus
	UART_HandleTypeDef *port; 					// HAL Serial Port handler
	/* FreeRTOS RX (CMSIS v2) components */
	osThreadId_t *ScaleCommTaskRx;			// Task Serial communication Rx
	osSemaphoreId_t *ScaleSemap_HandleRx;	// Sempahore to access the Data from/to modbus
	osMessageQueueId_t QueueSerialHandle;	// Queue Serial RX
	xTimerHandle xTimerT35;						// Timer RX Scale serial communication
	xTimerHandle xTimerTimeout;				// Timer MasterTimeout

	uint8_t au8Buffer[MAX_BUFFER]; 			// Serial buffer for communication
	uint8_t u8BufferSize;

	uint8_t dataRX;
	// uint8_t ReadOrWriteMode; // Read = 0, Write = 1

	/* FreeRTOS TX (CMSIS v2) components */
	osThreadId_t *ScaleCommTaskTx;			// Task Serial communication
	osSemaphoreId_t *ScaleSemap_HandleTx;	// Sempahore to access the Data from/to modbus
	unsigned char WeightReadReqAssci[4];
	uint8_t bit_read_req;
	uint8_t scale_read_position;
	osMessageQueueAttr_t* QueueAttr;
}
ScaleHandler_t;


ScaleHandler_t *tHandlers[MAX_M_HANDLERS];

void ScaleSerialCommInit(ScaleHandler_t *);
void ScaleSerialCommStart(ScaleHandler_t * );
void StartScaleRx_task(void *);
void StartScaleTx_task(void *);
void sendTxScale(ScaleHandler_t *);
int8_t getScaleRxBuffer(ScaleHandler_t *);
void vScaleTimerCallbackT35(TimerHandle_t *);
#endif /* INC_SCALESERIALCOMM_H_ */
