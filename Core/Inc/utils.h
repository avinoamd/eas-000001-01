/*
 * utils.h
 *
 *  Created on: 26 Jul 2021
 *      Author: avinoam.danieli
 */

#ifndef SRC_UTILS_H_
#define SRC_UTILS_H_

/*
 * dwt_stm32_delay.h
 *
 *  Created on: Jun 30, 2020
 *      Author: avinoam.danieli
 */

#ifndef DWT_STM32_DELAY_H_
#define DWT_STM32_DELAY_H_


/**
 * @brief Initializes DWT_Cycle_Count for DWT_Delay_us function
 * @return Error DWT counter
 * 1: DWT counter Error
 * 0: DWT counter works
 */
uint32_t DWT_Delay_Init(void);

/**
 * @brief This function provides a delay (in microseconds)
 * @param microseconds: delay in microseconds
 */
__STATIC_INLINE void DWT_Delay_us(volatile uint32_t microseconds)
{
 uint32_t clk_cycle_start = DWT->CYCCNT;
 /* Go to number of cycles for system */
 microseconds *= (HAL_RCC_GetHCLKFreq() / 1000000);
 /* Delay till end */
 while ((DWT->CYCCNT - clk_cycle_start) < microseconds);
}

#endif /* DWT_STM32_DELAY_H_ */


#endif /* SRC_UTILS_H_ */
