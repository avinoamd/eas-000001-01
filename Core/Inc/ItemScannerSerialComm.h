/*
 * ItemScannerSerialComm.h
 *
 *  Created on: MAy 6, 2021
 *      Author: segev.gofin
 */

#ifndef INC_ItemScannerSERIALCOMM_H_
#define INC_ItemScannerSERIALCOMM_H_



typedef struct
{
	uint16_t * Data; // pointer to Data from/to modbus
	UART_HandleTypeDef *port; 				// HAL Serial Port handler
	/* FreeRTOS RX (CMSIS v2) components */
	osThreadId_t *ItemScannerCommTaskRx;			// Task Serial communication Rx
	osSemaphoreId_t *ItemScannerSemap_HandleRx;	// Sempahore to access the Data from/to modbus
	osMessageQueueId_t QueueSerialHandle;	// Queue Serial RX
	xTimerHandle xTimerT35;					// Timer RX ItemScanner serial communication
	xTimerHandle xTimerTimeout;				// Timer MasterTimeout
	uint8_t au8Buffer[MAX_BUFFER]; 		// Serial buffer for communication
	uint8_t u8BufferSize;
	uint8_t dataRX;
	/* FreeRTOS TX (CMSIS v2) components */
	osSemaphoreId_t *ItemScannerSemap_HandleTx;	// Sempahore to access the Data from/to modbus
	uint8_t ItemScanner_read_position_MSB;
	uint8_t ItemScanner_read_position_B2;
	uint8_t ItemScanner_read_position_B3;
	uint8_t ItemScanner_read_position_LSB;
}
ItemScannerHandler_t;


ItemScannerHandler_t *IHandlers[MAX_M_HANDLERS];

void ItemScannerSerialCommInit(ItemScannerHandler_t *);
void ItemScannerSerialCommStart(ItemScannerHandler_t * );
void StartItemScannerRx_task(void *);
void StartItemScannerTx_task(void *);
void sendTxItemScanner(ItemScannerHandler_t *);
int8_t getItemScannerRxBuffer(ItemScannerHandler_t *);
void vItemScannerTimerCallbackT35(TimerHandle_t *);
#endif /* INC_SCALESERIALCOMM_H_ */
